﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ObslugaService1
{
    public partial class UstawieniaJoba : Form
    {
        string nazwaJoba;

        public UstawieniaJoba()
        {
            InitializeComponent();
        }
        public UstawieniaJoba(string a)
        {
            this.nazwaJoba = a;
            InitializeComponent();
        }

        private void UstawieniaJoba_Load(object sender, EventArgs e)
        {
            ScheduleGroupBox.Controls.Add(OneTimeRadioButton);
            ScheduleGroupBox.Controls.Add(RecurringRadioButton);
            //***************** Jeśli włącz jest zaznaczony to wszystko ma sie zablokować
            if (Włącz.Checked)
            {
                ScheduleGroupBox.Enabled = false;
                Włącz.Enabled = true;
            }
            try
            {
                wczytajzPliku();
                BlokujKontrolki();
                
            }
            catch (Exception ex)
            {
                ErrorDialog ed = new ErrorDialog();
                ed.errorLabel.Text = "Plik konfiguracyjny nie istnieje";
                ed.ShowDialog();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void Włącz_Click(object sender, EventArgs e)
        {

        }

        #region zapisz button i close button
        private void SaveButton_Click(object sender, EventArgs e)
        {
            zapisz();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            zapisz();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            zapisz();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            zapisz();
        }
        private void button3_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region zapisz do pliku
        public void zapisz()
        {
            #region zapisz poprzednia wersja
            //***************************************************************************************************************************
            //*********************** Zapisanie danych konfigurujących połączenie oraz Joba do pliku xml*********************************
            //***************************************************************************************************************************
            UstawieniaSerweraPlik usp = new UstawieniaSerweraPlik();
            ErrorDialog ed1 = new ErrorDialog();
            int licznik = 0;
            usp.SQLserverName1 = SQLServerNameTextBox.Text;
            usp.Username = userNameTextBOx.Text;
            usp.Password = passwordTextBox.Text;
            usp.JobName = jobNameTextBox.Text;
            usp.Description = DescriptionTextBox.Text;
            usp.ZapytanieSQL = QueryTextBox.Text;
            usp.NazwaBazyDanych = nazwaBazyDanychTextBox.Text;



            if (WARadioButton.Checked)
            {
                usp.Authentication = true;
            }
            else if (SQLSARadioButton2.Checked)
            {
                usp.Authentication = false;
            }

            if (OneTimeRadioButton.Checked) // zazaczony One Time
            {
                licznik = 0;
                // zapisanie do obiektu daty wykonania oraz ilosci wykonan (tu jedno wykonanie)     -   format dany dzien-miesiac-rok godzina:minuta:sekunda
                usp.Schedule = true;        // czy operacja ma być wykonywana raz czy wiele razy true - raz false kilka razy
                #region sprawdzenie czy pola czasu nie są puste oraz zapisaie daty i czasu do obiektu OneTimeDate
                if (OTGodzComboBox.Text == null)
                {
                    licznik++;
                }
                else if (OTMincomboBox.Text == null)
                {
                    licznik++;
                }
                else if (OTSekcomboBox.Text == null)
                {
                    licznik++;
                }
                if (licznik > 0)
                {
                    ErrorDialog ed = new ErrorDialog();
                    ed.errorLabel.Text = "Pola daty lub czasu jest puste!";
                    ed.ShowDialog();
                }
                else
                {   //zmiana formatu ze string na DateTime
                    try
                    {
                        string mojaData = OTdateTimePicker.Text + " " + OTGodzComboBox.Text + ":" + OTMincomboBox.Text + ":" + OTSekcomboBox.Text;
                        string stringObecnaData = String.Format("{0:d/M/yyyy HH:mm:ss}", mojaData);
                        usp.OneTimeDate = DateTime.Parse(stringObecnaData);
                        usp.RecuringStartDate1 = DateTime.Now.Date;
                    }
                    catch (Exception e)
                    {
                        ErrorDialog ed = new ErrorDialog();
                        ed.errorLabel.Text = e.ToString();
                    }
                }
                #endregion
            }
            else if (RecurringRadioButton.Checked)  //zaznaczony Recurring
            {
                usp.Schedule = false;
                licznik = 0;
                #region sprawdzenie czy pola czasu nie są puste
                if (RGodzcomboBox.Text == null)
                {
                    licznik++;
                }
                else if (RMincomboBox.Text == null)
                {
                    licznik++;
                }
                else if (RSekcomboBox.Text == null)
                {
                    licznik++;
                }
                if (licznik > 0)
                {
                    ErrorDialog ed = new ErrorDialog();
                    ed.errorLabel.Text = "Pola daty lub czasu jest puste!";
                    ed.ShowDialog();
                }
                else
                {
                    try
                    {
                        //zmiana formatu ze string na DateTime
                        string mojaData = RecuringdateTimePicker.Text + " " + RGodzcomboBox.Text + ":" + RMincomboBox.Text + ":" + RSekcomboBox.Text;
                        string stringObecnaData = String.Format("{0:d/M/yyyy HH:mm:ss}", mojaData);
                        usp.RecuringStartDate1 = DateTime.Parse(stringObecnaData);
                        usp.OneTimeDate = DateTime.Now.Date;
                    }
                    catch (Exception e)
                    {
                        ErrorDialog ed = new ErrorDialog();
                        ed.errorLabel.Text = e.ToString();
                    }
                }

                #endregion

                usp.Occurs = OccursTextBox.Text;    //ilość minut/dni itp co jaki ma sie wykonywać operacja
                usp.Time = TimeComboBox.Text;       //wybranie dnia/minuty/ mieisca itp 
            }
            if (Włącz.Checked)
            {
                usp.Wlaczony = true;
                //TextWriter zapis = new StreamWriter("C:\\ProgramData\\plikiKonfig\\Job.txt", true);
                //zapis.WriteLine(a.Remove(0, 10));
                //zapis.Close();
            }
            else if (!Włącz.Checked)
            {
                usp.Wlaczony = false;
            }

            //***************************************************************************************************************************
            
            string bezSmieci = nazwaJoba.Remove(0, 10); //funkcja usowa niepotrzebne elementy (od znaku o indeksie 0 do znaku o indeksie 10 (usuwa ThreadNode: 

            try
            {
                File.Delete("C:\\ProgramData\\Schedulerv1\\" + bezSmieci + ".xml");
                StreamWriter wr = new StreamWriter("C:\\ProgramData\\Schedulerv1\\" + bezSmieci + ".xml", true);   //skierowanie do lokalizacji pliku (true oznacza mozliwosc dopisywania)
                XmlSerializer serializer = new XmlSerializer(typeof(UstawieniaSerweraPlik));    //utworzenie obiektu o typie klasy jaka bedziemy zapisywali do obiektu

                serializer.Serialize(wr, usp);
                wr.Flush();
                wr.Close();
                this.Close();
            }
            catch (Exception e) //jesli wystąpi jakiś błąd to okienko poinformuje nas o tym
            {
                ErrorDialog ed = new ErrorDialog();
                ed.errorLabel.Text = "Plik jest używany przez inny proces. \nZamknij wszystkie programy i spróbuj ponownie";
                ed.ShowDialog();
            }

            StreamReader sr = new StreamReader("C:\\ProgramData\\Schedulerv1\\" + bezSmieci + ".xml");
            XmlSerializer serializerR = new XmlSerializer(typeof(UstawieniaSerweraPlik));
            UstawieniaSerweraPlik uspR = (UstawieniaSerweraPlik)serializerR.Deserialize(sr);

            // 
            List<String> uruJob = new List<string>();
            //Zapisanie wszystkich plikow do listy
            if (File.Exists("C:\\ProgramData\\Schedulerv1\\uruchomioneJob.txt"))
            {
                TextReader czytaj = new StreamReader("C:\\ProgramData\\Schedulerv1\\uruchomioneJob.txt");
                int iloscLinii = File.ReadAllLines("C:\\ProgramData\\Schedulerv1\\uruchomioneJob.txt").Length;
                uruJob.Clear();
                for (int i = 0; i < iloscLinii; i++)
                {
                    uruJob.Add(czytaj.ReadLine().ToString());
                }
                czytaj.Close();
            }
            //
            if (uspR.Wlaczony == true)   //warunek sprawdza czy dany job jest uruchomiony. Jesli jest to zapisuje go do pliku, z którego serwis będzie mógł odczytaćdane
            {
                uruJob.Add(bezSmieci);
            }
            else
            {
                for (int i = 0; i < uruJob.Count; i++)
                {
                    if (uruJob[i].Equals(bezSmieci))
                    {
                        uruJob.Remove(bezSmieci);
                    }
                }
            }
            File.Delete("C:\\ProgramData\\Schedulerv1\\uruchomioneJob.txt");
            TextWriter zapis = new StreamWriter("C:\\ProgramData\\Schedulerv1\\uruchomioneJob.txt", true);

            foreach (String uJ in uruJob)
            {
                zapis.WriteLine(uJ);

            }
            sr.Close();
            zapis.Close();
            #endregion
        }


        #endregion

      
   #region Blokuj kontroli
        public void BlokujKontrolki()
        {
            //******************************************************************************            
            //***********Jeśli jest zaznaczone wykonywanie tylko raz danej operacji to wszystkie kontrolki dla okresowego wykonania się blokują
            if (OneTimeRadioButton.Checked)
            {
                OTdateTimePicker.Enabled = true;
                OTGodzComboBox.Enabled = true;
                OTMincomboBox.Enabled = true;
                OTSekcomboBox.Enabled = true;

                RecuringdateTimePicker.Enabled = false;
                RGodzcomboBox.Enabled = false;
                RMincomboBox.Enabled = false;
                RSekcomboBox.Enabled = false;
                OccursTextBox.Enabled = false;
                TimeComboBox.Enabled = false;
            }
            else if (RecurringRadioButton.Checked)
            {
                OTdateTimePicker.Enabled = false;
                OTGodzComboBox.Enabled = false;
                OTMincomboBox.Enabled = false;
                OTSekcomboBox.Enabled = false;

                RecuringdateTimePicker.Enabled = true;
                RGodzcomboBox.Enabled = true;
                RMincomboBox.Enabled = true;
                RSekcomboBox.Enabled = true;
                OccursTextBox.Enabled = true;
                TimeComboBox.Enabled = true;
            }
            //******************************************************************************************************
        }

        #endregion
   #region Wczytaj z pliku
        public void wczytajzPliku()
        {
            string bezSmieci = nazwaJoba.Remove(0, 10);
            StreamReader sr = new StreamReader("C:\\ProgramData\\Schedulerv1\\" + bezSmieci + ".xml");
            XmlSerializer serializer = new XmlSerializer(typeof(UstawieniaSerweraPlik));
            UstawieniaSerweraPlik usp = (UstawieniaSerweraPlik)serializer.Deserialize(sr);

            try
            {
                SQLServerNameTextBox.Text = usp.SQLserverName1;
                nazwaBazyDanychTextBox.Text = usp.NazwaBazyDanych;
                userNameTextBOx.Text = usp.Username;
                passwordTextBox.Text = usp.Password;
                jobNameTextBox.Text = bezSmieci;
                DescriptionTextBox.Text = usp.Description;

                QueryTextBox.Text = usp.ZapytanieSQL.ToString();
                nazwaBazyDanychTextBox.Text = usp.NazwaBazyDanych.ToString();

                if (usp.Wlaczony == true)
                {
 
                    Włącz.Checked = true;
                    ScheduleGroupBox.Enabled = false;
                }
                else if (usp.Wlaczony == false)
                {
                    Włącz.Checked = false;
                    ScheduleGroupBox.Enabled = true;
                }

                if (usp.Schedule == true)
                {
                    OneTimeRadioButton.Checked = true;
                    RecurringRadioButton.Checked = false;
                    OTdateTimePicker.Text = usp.OneTimeDate.ToShortDateString();
                    OTGodzComboBox.Text = usp.OneTimeDate.Hour.ToString();
                    OTMincomboBox.Text = usp.OneTimeDate.Minute.ToString();
                    OTSekcomboBox.Text = usp.OneTimeDate.Second.ToString();
                }
                else if (usp.Schedule == false)
                {
                    OneTimeRadioButton.Checked = false;
                    RecurringRadioButton.Checked = true;
                    RecuringdateTimePicker.Text = usp.RecuringStartDate1.Date.ToShortDateString();
                    RGodzcomboBox.Text = usp.RecuringStartDate1.Hour.ToString();
                    RMincomboBox.Text = usp.RecuringStartDate1.Minute.ToString();
                    RSekcomboBox.Text = usp.RecuringStartDate1.Second.ToString();
                    OccursTextBox.Text = usp.Occurs;
                    TimeComboBox.Text = usp.Time;
                }


                if (usp.Authentication == true)
                {
                    WARadioButton.Checked = true;
                    SQLSARadioButton2.Checked = false;
                    userNameTextBOx.Enabled = false;
                    passwordTextBox.Enabled = false;
                }
                else if (usp.Authentication == false)
                {
                    WARadioButton.Checked = false;
                    SQLSARadioButton2.Checked = true;
                    userNameTextBOx.Enabled = true;
                    passwordTextBox.Enabled = true;
                }
            }
            catch (Exception e)
            {
                ErrorDialog ed = new ErrorDialog();
                ed.errorLabel.Text = "Wystąpił błąd z plikiem konfiguracyjnym. Usuń plik i spróbuj ponownie.\n"  + " " + e.ToString(); ;
                ed.ShowDialog();
            }



            //     OTdateTimePicker.D

            sr.Close();
        }


        #endregion

        private void Włącz_Click_1(object sender, EventArgs e)
        {
            if (Włącz.Checked)
            {
                ScheduleGroupBox.Enabled = false;
            }
            else if (!Włącz.Checked)
            {
                ScheduleGroupBox.Enabled = true;
            }
        }

        private void WARadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (WARadioButton.Checked)
            {
                userNameTextBOx.Enabled = false;
                passwordTextBox.Enabled = false;
            }
            else if (SQLSARadioButton2.Checked)
            {
                userNameTextBOx.Enabled = true;
                passwordTextBox.Enabled = true;
            }
        }

        private void RecurringRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (OneTimeRadioButton.Checked)
            {
                OTdateTimePicker.Enabled = true;
                OTGodzComboBox.Enabled = true;
                OTMincomboBox.Enabled = true;
                OTSekcomboBox.Enabled = true;

                RecuringdateTimePicker.Enabled = false;
                RGodzcomboBox.Enabled = false;
                RMincomboBox.Enabled = false;
                RSekcomboBox.Enabled = false;
                OccursTextBox.Enabled = false;
                TimeComboBox.Enabled = false;
            }
            else if (RecurringRadioButton.Checked)
            {
                OTdateTimePicker.Enabled = false;
                OTGodzComboBox.Enabled = false;
                OTMincomboBox.Enabled = false;
                OTSekcomboBox.Enabled = false;

                RecuringdateTimePicker.Enabled = true;
                RGodzcomboBox.Enabled = true;
                RMincomboBox.Enabled = true;
                RSekcomboBox.Enabled = true;
                OccursTextBox.Enabled = true;
                TimeComboBox.Enabled = true;
            }
        }


    }
}

