﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;

namespace ObslugaService1
{
    public partial class NowyJob : Form
    {
        public NowyJob()
        {
            InitializeComponent();
        }
#region Tworzenie nowego Joba
        private void ZapiszJobButton_Click(object sender, EventArgs e)
        {

                TextWriter zapis = new StreamWriter("C:\\ProgramData\\Schedulerv1\\nazwyJob.txt", true);
                zapis.WriteLine(nameJobTextBox.Text);
                zapis.Close();

            UstawieniaSerweraPlik  usp = new UstawieniaSerweraPlik();
            StreamWriter wr = new StreamWriter("C:\\ProgramData\\Schedulerv1\\" + nameJobTextBox.Text + ".xml", true);
            XmlSerializer serializer = new XmlSerializer(typeof(UstawieniaSerweraPlik));

            ustawDefault(usp, wr, serializer);
            this.Close();
        }

        public void ustawDefault(UstawieniaSerweraPlik usp, StreamWriter wr, XmlSerializer serializer)
        {
            //ustawienie domyslnych pol kontorlek - tu wszystkie puste

            UstawieniaJoba uj = new UstawieniaJoba();
            DateTime dt = new DateTime();
            usp.SQLserverName1 = "";
            usp.Username = "";
            usp.Password = "";
            usp.JobName = "";
            usp.Description = "";
            usp.ZapytanieSQL = "";
            usp.NazwaBazyDanych = "";
            usp.Authentication = true;
            usp.Schedule = true;
            usp.OneTimeDate = DateTime.Now;
            usp.RecuringStartDate1 = DateTime.Now;
            usp.Occurs = "";
            usp.Time = "";
            usp.Wlaczony = true;
            serializer.Serialize(wr, usp);
            wr.Flush();
            wr.Close();
        }
        #endregion



    }
}
