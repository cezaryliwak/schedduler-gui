﻿namespace ObslugaService1
{
    partial class NowyJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ZapiszJobButton = new System.Windows.Forms.Button();
            this.nameJobTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ZapiszJobButton
            // 
            this.ZapiszJobButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ZapiszJobButton.Location = new System.Drawing.Point(0, 37);
            this.ZapiszJobButton.Name = "ZapiszJobButton";
            this.ZapiszJobButton.Size = new System.Drawing.Size(284, 23);
            this.ZapiszJobButton.TabIndex = 0;
            this.ZapiszJobButton.Text = "Zapisz";
            this.ZapiszJobButton.UseVisualStyleBackColor = true;
            this.ZapiszJobButton.Click += new System.EventHandler(this.ZapiszJobButton_Click);
            // 
            // nameJobTextBox
            // 
            this.nameJobTextBox.Location = new System.Drawing.Point(0, 16);
            this.nameJobTextBox.Name = "nameJobTextBox";
            this.nameJobTextBox.Size = new System.Drawing.Size(284, 20);
            this.nameJobTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Podaj nazwę joba";
            // 
            // NowyJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 60);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nameJobTextBox);
            this.Controls.Add(this.ZapiszJobButton);
            this.Name = "NowyJob";
            this.Text = "NowyJob";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ZapiszJobButton;
        private System.Windows.Forms.TextBox nameJobTextBox;
        private System.Windows.Forms.Label label1;
    }
}