﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using System.Diagnostics;

namespace ObslugaService1

{
    public partial class Form1 : Form
    {
        public String lokalizacjaNazwyJob = "C:\\ProgramData\\Schedulerv1\\nazwyJob.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!File.Exists(lokalizacjaNazwyJob))//sprawdza czy plik nazwyJob istnieje 
                                                     //jesli nie to tworzy ten plik
            {
                TextWriter zapis = new StreamWriter(lokalizacjaNazwyJob, true);
                zapis.Close();
            }
            treeView1.Nodes.Add("Serwery");
            TextReader czytaj = new StreamReader(lokalizacjaNazwyJob);
            string tekst;
            int iloscLinii = File.ReadAllLines(lokalizacjaNazwyJob).Length;
            for(int i=0;i<iloscLinii; i++)
            {
                tekst = czytaj.ReadLine();
                treeView1.Nodes[0].Nodes.Add(tekst);
            }
            czytaj.Close();
        }

        private void nowyJobToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NowyJob nj = new NowyJob();
            nj.ShowDialog();
        }

        private void treeView1_MouseDown(object sender, MouseEventArgs e)
        {
            // switch odpowiada za rozróżnianie lewego i prawego przycisku myszy
            switch (e.Button)
            {
                case (MouseButtons.Right):
                    {
                        //******************************************************************************
                        //****************** wyswietlanie menu po kliknieciu prawego przycisku myszy****
                        //**************************co ma być w ContextMenuStrip ustawiamy w desinger***
                        treeView1.SelectedNode = treeView1.GetNodeAt(e.X, e.Y);
                        if (treeView1.SelectedNode != null)
                        {
                            cMs.Show(treeView1, e.Location);
                        }
                        //******************************************************************************
                        break;
                    }
            }
            //************************************************************************
        }

        private void edytujToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UstawieniaJoba uj = new UstawieniaJoba(treeView1.SelectedNode.ToString());
            uj.ShowDialog();
        }

        private void usuńToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextReader czytaj = new StreamReader(lokalizacjaNazwyJob);
            String tekst;
            List<String> listaSerwerow = new List<string>();
            string bezSmieci = treeView1.SelectedNode.ToString().Remove(0, 10);

            int iloscLinii = File.ReadAllLines(lokalizacjaNazwyJob).Length;
            //wczytanie danych z pliku do listy
            for (int i = 0; i < iloscLinii; i++)
            {
                listaSerwerow.Add(czytaj.ReadLine());
            }
            czytaj.Close();
            //--------------------------------
            //Usuwanie serwera z List<String> listaplikow
            TextWriter tw = new StreamWriter(lokalizacjaNazwyJob);
            for (int i = 0; i < listaSerwerow.Count(); i++)
            {
                if (bezSmieci.Equals(listaSerwerow[i]))
                {
                    listaSerwerow.RemoveAt(i);
                }
            }
            //Zapisanie pozostałych nazw serwerow do plikow
            for (int i = 0; i < listaSerwerow.Count(); i++)
            {
                tw.WriteLine(listaSerwerow[i].ToString());
            }
            File.Delete("C:\\ProgramData\\Schedulerv1\\" + bezSmieci + ".xml");
            tw.Close();
        }


        private void treeView1_DoubleClick(object sender, EventArgs e)
        {


                StreamReader sr = new StreamReader("C:\\ProgramData\\Schedulerv1\\" + treeView1.SelectedNode.ToString().Remove(0, 10) + ".xml");
                XmlSerializer serializer = new XmlSerializer(typeof(UstawieniaSerweraPlik));
                UstawieniaSerweraPlik usp = (UstawieniaSerweraPlik)serializer.Deserialize(sr);

                daneDataGridView.Rows[0].Cells[0].Value = usp.SQLserverName1.ToString();


                daneDataGridView.Rows[0].Cells[1].Value = usp.NazwaBazyDanych.ToString();
                daneDataGridView.Rows[0].Cells[2].Value = usp.JobName.ToString();
            
                if (usp.Schedule == true)
                {
                    daneDataGridView.Rows[0].Cells[3].Value = usp.OneTimeDate.ToString();
                }
                else if (usp.Schedule == false)
                {
                    daneDataGridView.Rows[0].Cells[3].Value = usp.RecuringStartDate1.ToString();
                }
                if (usp.Wlaczony == true)
                {
                    daneDataGridView.Rows[0].Cells[4].Value = "Włączony";

                }
                else if (usp.Wlaczony == false)
                {
                    daneDataGridView.Rows[0].Cells[4].Value = "Wyłączony";
                    daneDataGridView.Columns["Włączony"].HeaderText = "Wyłączony";
                }


            richTextBox2.Text = usp.Description.ToString();
                richTextBox1.Text = usp.ZapytanieSQL.ToString();
                sr.Close();
  

        }

        private void restartServiceButton_Click(object sender, EventArgs e)
        {
            // Funkcja ma restartować serwis po dodaniu lub włączeniu Joba
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = "cmd";
                process.StartInfo.Arguments = "/c net stop \"SchedulerService1\" & net start \"SchedulerService1\"";
                process.Start();


            }
            catch(Exception ex)
            {
                ErrorDialog ed = new ErrorDialog();
                ed.errorLabel.Text = ex.ToString();
                ed.ShowDialog();
            }



        }
    }
}
