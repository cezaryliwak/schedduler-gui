﻿namespace ObslugaService1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nowyJobToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.daneDataGridView = new System.Windows.Forms.DataGridView();
            this.nazwaSerwera = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazwaBazyDanych = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazwaJob = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nastepneUruchomienie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wlaczony = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.cMs = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.edytujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuńToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartServiceButton = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daneDataGridView)).BeginInit();
            this.cMs.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(730, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nowyJobToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // nowyJobToolStripMenuItem
            // 
            this.nowyJobToolStripMenuItem.Name = "nowyJobToolStripMenuItem";
            this.nowyJobToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.nowyJobToolStripMenuItem.Text = "Nowy job";
            this.nowyJobToolStripMenuItem.Click += new System.EventHandler(this.nowyJobToolStripMenuItem_Click);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 27);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(165, 232);
            this.treeView1.TabIndex = 1;
            this.treeView1.DoubleClick += new System.EventHandler(this.treeView1_DoubleClick);
            this.treeView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseDown);
            // 
            // daneDataGridView
            // 
            this.daneDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.daneDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nazwaSerwera,
            this.nazwaBazyDanych,
            this.nazwaJob,
            this.nastepneUruchomienie,
            this.wlaczony});
            this.daneDataGridView.Location = new System.Drawing.Point(184, 28);
            this.daneDataGridView.Name = "daneDataGridView";
            this.daneDataGridView.Size = new System.Drawing.Size(542, 74);
            this.daneDataGridView.TabIndex = 2;
            // 
            // nazwaSerwera
            // 
            this.nazwaSerwera.HeaderText = "Nazwa Serwera";
            this.nazwaSerwera.Name = "nazwaSerwera";
            // 
            // nazwaBazyDanych
            // 
            this.nazwaBazyDanych.HeaderText = "Baza danych";
            this.nazwaBazyDanych.Name = "nazwaBazyDanych";
            // 
            // nazwaJob
            // 
            this.nazwaJob.HeaderText = "Job";
            this.nazwaJob.Name = "nazwaJob";
            // 
            // nastepneUruchomienie
            // 
            this.nastepneUruchomienie.HeaderText = "Nastepne uruchomienie";
            this.nastepneUruchomienie.Name = "nastepneUruchomienie";
            // 
            // wlaczony
            // 
            this.wlaczony.HeaderText = "Włączony";
            this.wlaczony.Name = "wlaczony";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(184, 109);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(308, 150);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // cMs
            // 
            this.cMs.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.edytujToolStripMenuItem,
            this.usuńToolStripMenuItem});
            this.cMs.Name = "cMs";
            this.cMs.Size = new System.Drawing.Size(108, 48);
            // 
            // edytujToolStripMenuItem
            // 
            this.edytujToolStripMenuItem.Name = "edytujToolStripMenuItem";
            this.edytujToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.edytujToolStripMenuItem.Text = "Edytuj";
            this.edytujToolStripMenuItem.Click += new System.EventHandler(this.edytujToolStripMenuItem_Click);
            // 
            // usuńToolStripMenuItem
            // 
            this.usuńToolStripMenuItem.Name = "usuńToolStripMenuItem";
            this.usuńToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.usuńToolStripMenuItem.Text = "Usuń";
            this.usuńToolStripMenuItem.Click += new System.EventHandler(this.usuńToolStripMenuItem_Click);
            // 
            // restartServiceButton
            // 
            this.restartServiceButton.Location = new System.Drawing.Point(12, 271);
            this.restartServiceButton.Name = "restartServiceButton";
            this.restartServiceButton.Size = new System.Drawing.Size(165, 23);
            this.restartServiceButton.TabIndex = 4;
            this.restartServiceButton.Text = "Zapisz zmiany";
            this.restartServiceButton.UseVisualStyleBackColor = true;
            this.restartServiceButton.Click += new System.EventHandler(this.restartServiceButton_Click);
            // 
            // richTextBox2
            // 
            this.richTextBox2.HideSelection = false;
            this.richTextBox2.Location = new System.Drawing.Point(498, 109);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(228, 150);
            this.richTextBox2.TabIndex = 5;
            this.richTextBox2.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 306);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.restartServiceButton);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.daneDataGridView);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daneDataGridView)).EndInit();
            this.cMs.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nowyJobToolStripMenuItem;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.DataGridView daneDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwaSerwera;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwaBazyDanych;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwaJob;
        private System.Windows.Forms.DataGridViewTextBoxColumn nastepneUruchomienie;
        private System.Windows.Forms.DataGridViewTextBoxColumn wlaczony;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ContextMenuStrip cMs;
        private System.Windows.Forms.ToolStripMenuItem edytujToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuńToolStripMenuItem;
        private System.Windows.Forms.Button restartServiceButton;
        private System.Windows.Forms.RichTextBox richTextBox2;
    }
}

