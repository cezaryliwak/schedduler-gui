﻿namespace ObslugaService1
{
    partial class UstawieniaJoba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.WARadioButton = new System.Windows.Forms.RadioButton();
            this.SQLSARadioButton2 = new System.Windows.Forms.RadioButton();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.userNameTextBOx = new System.Windows.Forms.TextBox();
            this.nazwaBazyDanychTextBox = new System.Windows.Forms.TextBox();
            this.SQLServerNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.jobNameTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.Włącz = new System.Windows.Forms.CheckBox();
            this.ScheduleGroupBox = new System.Windows.Forms.GroupBox();
            this.TimeComboBox = new System.Windows.Forms.ComboBox();
            this.OccursTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.RSekcomboBox = new System.Windows.Forms.ComboBox();
            this.RMincomboBox = new System.Windows.Forms.ComboBox();
            this.RGodzcomboBox = new System.Windows.Forms.ComboBox();
            this.RecuringdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.OTSekcomboBox = new System.Windows.Forms.ComboBox();
            this.OTMincomboBox = new System.Windows.Forms.ComboBox();
            this.OTGodzComboBox = new System.Windows.Forms.ComboBox();
            this.OTdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.RecurringRadioButton = new System.Windows.Forms.RadioButton();
            this.OneTimeRadioButton = new System.Windows.Forms.RadioButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.QueryTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.ScheduleGroupBox.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(486, 256);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.CloseButton);
            this.tabPage3.Controls.Add(this.SaveButton);
            this.tabPage3.Controls.Add(this.passwordTextBox);
            this.tabPage3.Controls.Add(this.userNameTextBOx);
            this.tabPage3.Controls.Add(this.nazwaBazyDanychTextBox);
            this.tabPage3.Controls.Add(this.SQLServerNameTextBox);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(478, 230);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Połączenie";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.WARadioButton);
            this.groupBox1.Controls.Add(this.SQLSARadioButton2);
            this.groupBox1.Location = new System.Drawing.Point(181, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(277, 62);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // WARadioButton
            // 
            this.WARadioButton.AutoSize = true;
            this.WARadioButton.Checked = true;
            this.WARadioButton.Location = new System.Drawing.Point(11, 10);
            this.WARadioButton.Name = "WARadioButton";
            this.WARadioButton.Size = new System.Drawing.Size(140, 17);
            this.WARadioButton.TabIndex = 10;
            this.WARadioButton.TabStop = true;
            this.WARadioButton.Text = "Windows Authentication";
            this.WARadioButton.UseVisualStyleBackColor = true;
            this.WARadioButton.CheckedChanged += new System.EventHandler(this.WARadioButton_CheckedChanged);
            // 
            // SQLSARadioButton2
            // 
            this.SQLSARadioButton2.AutoSize = true;
            this.SQLSARadioButton2.Location = new System.Drawing.Point(11, 33);
            this.SQLSARadioButton2.Name = "SQLSARadioButton2";
            this.SQLSARadioButton2.Size = new System.Drawing.Size(151, 17);
            this.SQLSARadioButton2.TabIndex = 11;
            this.SQLSARadioButton2.Text = "SQL Server Authentication";
            this.SQLSARadioButton2.UseVisualStyleBackColor = true;
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(6, 170);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 9;
            this.CloseButton.Text = "Wyjdź";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(6, 144);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 8;
            this.SaveButton.Text = "Zapisz";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(181, 172);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(277, 20);
            this.passwordTextBox.TabIndex = 7;
            // 
            // userNameTextBOx
            // 
            this.userNameTextBOx.Location = new System.Drawing.Point(181, 146);
            this.userNameTextBOx.Name = "userNameTextBOx";
            this.userNameTextBOx.Size = new System.Drawing.Size(277, 20);
            this.userNameTextBOx.TabIndex = 6;
            // 
            // nazwaBazyDanychTextBox
            // 
            this.nazwaBazyDanychTextBox.Location = new System.Drawing.Point(181, 52);
            this.nazwaBazyDanychTextBox.Name = "nazwaBazyDanychTextBox";
            this.nazwaBazyDanychTextBox.Size = new System.Drawing.Size(277, 20);
            this.nazwaBazyDanychTextBox.TabIndex = 5;
            // 
            // SQLServerNameTextBox
            // 
            this.SQLServerNameTextBox.Location = new System.Drawing.Point(181, 19);
            this.SQLServerNameTextBox.Name = "SQLServerNameTextBox";
            this.SQLServerNameTextBox.Size = new System.Drawing.Size(277, 20);
            this.SQLServerNameTextBox.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(97, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Username";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(97, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Hasło";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nazwa bazy danych";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SQL Serwer";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.DescriptionTextBox);
            this.tabPage1.Controls.Add(this.jobNameTextBox);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(478, 230);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Job";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(268, 167);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Wyjdź";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(151, 167);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Zapisz";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.Location = new System.Drawing.Point(136, 50);
            this.DescriptionTextBox.Multiline = true;
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(324, 89);
            this.DescriptionTextBox.TabIndex = 5;
            // 
            // jobNameTextBox
            // 
            this.jobNameTextBox.Enabled = false;
            this.jobNameTextBox.Location = new System.Drawing.Point(136, 16);
            this.jobNameTextBox.Name = "jobNameTextBox";
            this.jobNameTextBox.Size = new System.Drawing.Size(324, 20);
            this.jobNameTextBox.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Description";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Name";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.Włącz);
            this.tabPage2.Controls.Add(this.ScheduleGroupBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(478, 230);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "Powtórzenia";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(275, 195);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 21;
            this.button3.Text = "Wyjdź";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(153, 195);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 20;
            this.button4.Text = "Zapisz";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // Włącz
            // 
            this.Włącz.AutoSize = true;
            this.Włącz.Checked = true;
            this.Włącz.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Włącz.Location = new System.Drawing.Point(201, 21);
            this.Włącz.Name = "Włącz";
            this.Włącz.Size = new System.Drawing.Size(58, 17);
            this.Włącz.TabIndex = 19;
            this.Włącz.Text = "Włącz";
            this.Włącz.UseVisualStyleBackColor = true;
            this.Włącz.Click += new System.EventHandler(this.Włącz_Click_1);
            // 
            // ScheduleGroupBox
            // 
            this.ScheduleGroupBox.Controls.Add(this.TimeComboBox);
            this.ScheduleGroupBox.Controls.Add(this.OccursTextBox);
            this.ScheduleGroupBox.Controls.Add(this.label7);
            this.ScheduleGroupBox.Controls.Add(this.label8);
            this.ScheduleGroupBox.Controls.Add(this.RSekcomboBox);
            this.ScheduleGroupBox.Controls.Add(this.RMincomboBox);
            this.ScheduleGroupBox.Controls.Add(this.RGodzcomboBox);
            this.ScheduleGroupBox.Controls.Add(this.RecuringdateTimePicker);
            this.ScheduleGroupBox.Controls.Add(this.OTSekcomboBox);
            this.ScheduleGroupBox.Controls.Add(this.OTMincomboBox);
            this.ScheduleGroupBox.Controls.Add(this.OTGodzComboBox);
            this.ScheduleGroupBox.Controls.Add(this.OTdateTimePicker);
            this.ScheduleGroupBox.Controls.Add(this.RecurringRadioButton);
            this.ScheduleGroupBox.Controls.Add(this.OneTimeRadioButton);
            this.ScheduleGroupBox.Location = new System.Drawing.Point(6, 44);
            this.ScheduleGroupBox.Name = "ScheduleGroupBox";
            this.ScheduleGroupBox.Size = new System.Drawing.Size(466, 145);
            this.ScheduleGroupBox.TabIndex = 1;
            this.ScheduleGroupBox.TabStop = false;
            // 
            // TimeComboBox
            // 
            this.TimeComboBox.FormattingEnabled = true;
            this.TimeComboBox.Items.AddRange(new object[] {
            "Minute",
            "Hour",
            "Day"});
            this.TimeComboBox.Location = new System.Drawing.Point(321, 108);
            this.TimeComboBox.Name = "TimeComboBox";
            this.TimeComboBox.Size = new System.Drawing.Size(122, 21);
            this.TimeComboBox.TabIndex = 16;
            // 
            // OccursTextBox
            // 
            this.OccursTextBox.Location = new System.Drawing.Point(182, 109);
            this.OccursTextBox.Name = "OccursTextBox";
            this.OccursTextBox.Size = new System.Drawing.Size(82, 20);
            this.OccursTextBox.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(88, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Occurs every";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(103, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Start Date";
            // 
            // RSekcomboBox
            // 
            this.RSekcomboBox.FormattingEnabled = true;
            this.RSekcomboBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.RSekcomboBox.Location = new System.Drawing.Point(411, 64);
            this.RSekcomboBox.Name = "RSekcomboBox";
            this.RSekcomboBox.Size = new System.Drawing.Size(37, 21);
            this.RSekcomboBox.TabIndex = 12;
            // 
            // RMincomboBox
            // 
            this.RMincomboBox.FormattingEnabled = true;
            this.RMincomboBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.RMincomboBox.Location = new System.Drawing.Point(370, 64);
            this.RMincomboBox.Name = "RMincomboBox";
            this.RMincomboBox.Size = new System.Drawing.Size(35, 21);
            this.RMincomboBox.TabIndex = 11;
            // 
            // RGodzcomboBox
            // 
            this.RGodzcomboBox.FormattingEnabled = true;
            this.RGodzcomboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "00"});
            this.RGodzcomboBox.Location = new System.Drawing.Point(321, 64);
            this.RGodzcomboBox.Name = "RGodzcomboBox";
            this.RGodzcomboBox.Size = new System.Drawing.Size(43, 21);
            this.RGodzcomboBox.TabIndex = 10;
            // 
            // RecuringdateTimePicker
            // 
            this.RecuringdateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.RecuringdateTimePicker.Location = new System.Drawing.Point(182, 65);
            this.RecuringdateTimePicker.Name = "RecuringdateTimePicker";
            this.RecuringdateTimePicker.Size = new System.Drawing.Size(82, 20);
            this.RecuringdateTimePicker.TabIndex = 9;
            // 
            // OTSekcomboBox
            // 
            this.OTSekcomboBox.FormattingEnabled = true;
            this.OTSekcomboBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.OTSekcomboBox.Location = new System.Drawing.Point(411, 19);
            this.OTSekcomboBox.Name = "OTSekcomboBox";
            this.OTSekcomboBox.Size = new System.Drawing.Size(37, 21);
            this.OTSekcomboBox.TabIndex = 5;
            // 
            // OTMincomboBox
            // 
            this.OTMincomboBox.FormattingEnabled = true;
            this.OTMincomboBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.OTMincomboBox.Location = new System.Drawing.Point(370, 19);
            this.OTMincomboBox.Name = "OTMincomboBox";
            this.OTMincomboBox.Size = new System.Drawing.Size(35, 21);
            this.OTMincomboBox.TabIndex = 4;
            // 
            // OTGodzComboBox
            // 
            this.OTGodzComboBox.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.OTGodzComboBox.FormattingEnabled = true;
            this.OTGodzComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "00"});
            this.OTGodzComboBox.Location = new System.Drawing.Point(321, 19);
            this.OTGodzComboBox.Name = "OTGodzComboBox";
            this.OTGodzComboBox.Size = new System.Drawing.Size(43, 21);
            this.OTGodzComboBox.TabIndex = 3;
            // 
            // OTdateTimePicker
            // 
            this.OTdateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.OTdateTimePicker.Location = new System.Drawing.Point(182, 20);
            this.OTdateTimePicker.Name = "OTdateTimePicker";
            this.OTdateTimePicker.Size = new System.Drawing.Size(82, 20);
            this.OTdateTimePicker.TabIndex = 2;
            // 
            // RecurringRadioButton
            // 
            this.RecurringRadioButton.AutoSize = true;
            this.RecurringRadioButton.Location = new System.Drawing.Point(41, 43);
            this.RecurringRadioButton.Name = "RecurringRadioButton";
            this.RecurringRadioButton.Size = new System.Drawing.Size(71, 17);
            this.RecurringRadioButton.TabIndex = 1;
            this.RecurringRadioButton.Text = "Recurring";
            this.RecurringRadioButton.UseVisualStyleBackColor = true;
            this.RecurringRadioButton.CheckedChanged += new System.EventHandler(this.RecurringRadioButton_CheckedChanged);
            // 
            // OneTimeRadioButton
            // 
            this.OneTimeRadioButton.AutoSize = true;
            this.OneTimeRadioButton.Checked = true;
            this.OneTimeRadioButton.Location = new System.Drawing.Point(41, 20);
            this.OneTimeRadioButton.Name = "OneTimeRadioButton";
            this.OneTimeRadioButton.Size = new System.Drawing.Size(68, 17);
            this.OneTimeRadioButton.TabIndex = 0;
            this.OneTimeRadioButton.TabStop = true;
            this.OneTimeRadioButton.Text = "OneTime";
            this.OneTimeRadioButton.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button5);
            this.tabPage4.Controls.Add(this.button6);
            this.tabPage4.Controls.Add(this.QueryTextBox);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(478, 230);
            this.tabPage4.TabIndex = 5;
            this.tabPage4.Text = "SQL";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(254, 201);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 15;
            this.button5.Text = "Wyjdź";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(137, 201);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 14;
            this.button6.Text = "Zapisz";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // QueryTextBox
            // 
            this.QueryTextBox.Location = new System.Drawing.Point(104, 6);
            this.QueryTextBox.Multiline = true;
            this.QueryTextBox.Name = "QueryTextBox";
            this.QueryTextBox.Size = new System.Drawing.Size(371, 184);
            this.QueryTextBox.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "SQL Query:";
            // 
            // UstawieniaJoba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 280);
            this.Controls.Add(this.tabControl1);
            this.Name = "UstawieniaJoba";
            this.Text = "UstawieniaJoba";
            this.Load += new System.EventHandler(this.UstawieniaJoba_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ScheduleGroupBox.ResumeLayout(false);
            this.ScheduleGroupBox.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton WARadioButton;
        private System.Windows.Forms.RadioButton SQLSARadioButton2;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.TextBox userNameTextBOx;
        private System.Windows.Forms.TextBox nazwaBazyDanychTextBox;
        private System.Windows.Forms.TextBox SQLServerNameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private System.Windows.Forms.TextBox jobNameTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox Włącz;
        private System.Windows.Forms.GroupBox ScheduleGroupBox;
        private System.Windows.Forms.ComboBox TimeComboBox;
        private System.Windows.Forms.TextBox OccursTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox RSekcomboBox;
        private System.Windows.Forms.ComboBox RMincomboBox;
        private System.Windows.Forms.ComboBox RGodzcomboBox;
        private System.Windows.Forms.DateTimePicker RecuringdateTimePicker;
        private System.Windows.Forms.ComboBox OTSekcomboBox;
        private System.Windows.Forms.ComboBox OTMincomboBox;
        public System.Windows.Forms.ComboBox OTGodzComboBox;
        private System.Windows.Forms.DateTimePicker OTdateTimePicker;
        private System.Windows.Forms.RadioButton RecurringRadioButton;
        private System.Windows.Forms.RadioButton OneTimeRadioButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox QueryTextBox;
        private System.Windows.Forms.Label label9;
    }
}